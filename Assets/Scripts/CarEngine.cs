using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEngine : MonoBehaviour {

    public Transform path;
    public float maxSteerAngle = 45f;
    public float maxMotorTorque = 800f;
    public float currentSpeed;
    public float tooMuchSpeed = 500;
    public float maxSpeed = 300f;
    public Vector3 centerOfMass;
    public Rigidbody carRigidBody;

    public WheelCollider wheelFR;
    public WheelCollider wheelFL;


    private List<Transform> nodes;
    private int currentNode = 0;


    // Start is called before the first frame update
    private void Start() {
        GetComponent<Rigidbody>().centerOfMass = centerOfMass;

        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++) {
            if (pathTransforms[i] != path.transform) {
               nodes.Add(pathTransforms[i]); 
            }
        }
    }

    // Update is called once per frame
    private void FixedUpdate() {
        ApplySteer();
        Drive();
        CheckWaypointDistance();
    }

    // Hace que las ruedas apunten a la direccion del nodo
    private void ApplySteer() {
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[currentNode].position);
        
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;

        wheelFL.steerAngle = newSteer;
        wheelFR.steerAngle = newSteer;

    }

    // Fuerza y velocidad del coche
    private void Drive() {

        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 100;
    
        
        if (currentSpeed < maxSpeed) {
            wheelFL.motorTorque = maxMotorTorque;
            wheelFR.motorTorque = maxMotorTorque;
        } else {
            wheelFL.motorTorque = 0;
            wheelFR.motorTorque = 0;
        }

        if (currentSpeed >= tooMuchSpeed) {
            carRigidBody.Sleep();
        }
        
        
    }

    // Calcula la distancia del proximo nodo
    private void CheckWaypointDistance() {

        if (Vector3.Distance(transform.position, nodes[currentNode].position) < 4f) {
             if (currentNode == nodes.Count -1) {
                currentNode = 0;
             }
             else {
                 currentNode++;
             }
        }
        print(currentNode);


    }


}
