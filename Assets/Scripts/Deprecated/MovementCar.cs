using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCar : MonoBehaviour
{

    public float carSpeed = 60f;
    public float carRotationSpeed = 10.0f;


    // Update is called once per frame
    void Update()
    {

        float translation = Input.GetAxis("Vertical") * carSpeed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * carRotationSpeed * Time.deltaTime;

        transform.Translate(0, 0, translation);

        transform.Rotate(0, rotation, 0);
        
    }
}
