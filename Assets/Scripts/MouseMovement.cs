using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour {

    // Sensivilidad del ratón al movimiento
    public Vector2 sensitivity; // [x] = 1000; [y] = -1000;

    //  La Rotación basada en la aceleración 
    public Vector2 acceleration; // [x] = 1000; [y] = 1000;

    // La espera hasta resetear el valor
    public float inputLagPeriod; // 0.0005;

    // El maximo angulo posible en grados
    public float maxVerticalAngleFromHorizon; // 80;


    // Rotación actual en grados
    private Vector2 rotation;
    // La velocidad de Rotación
    private Vector2 velocity;
    // El ultimo no-0 Recibido
    private Vector2 lastInputEvent;
    // El tiempo hasta el ultimo no-0 recibido
    private float inputLagTimer;

    // Consigue los valores del Ratón
    private Vector2 GetInput() {

        // Añade el tiempo de Lag
        inputLagTimer += Time.deltaTime;

        // Consigue los valores del Ratón
        Vector2 input = new Vector2(
            Input.GetAxis("Mouse X"),
            Input.GetAxis("Mouse Y")
        );

        // 
        if ((Mathf.Approximately(0, input.x) && Mathf.Approximately(0, input.y)) == false ||
            inputLagTimer >= inputLagPeriod ) {
            
            lastInputEvent = input;
            inputLagTimer = 0;
        }


        return lastInputEvent;
    }


    //
    private float ClampVerticalAngle(float angle){
        return Mathf.Clamp(angle, -maxVerticalAngleFromHorizon, maxVerticalAngleFromHorizon);
    }

    // Update per Frames
    private void Update() {

        // La velocidad del movimiento
        Vector2 wantedVelocity = GetInput() * sensitivity;

        // Calcula la nueva rotación
        velocity = new Vector2(
            Mathf.MoveTowards(velocity.x, wantedVelocity.x, acceleration.x * Time.deltaTime),
            Mathf.MoveTowards(velocity.y, wantedVelocity.y, acceleration.y * Time.deltaTime)
        );

        rotation += velocity * Time.deltaTime;
        rotation.y = ClampVerticalAngle(rotation.y);
    
        // Convierte la rotación según los angulos
        transform.localEulerAngles = new Vector3(rotation.y, rotation.x, 0);
    
    }



}
